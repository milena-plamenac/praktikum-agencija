<?php

    // array holding allowed Origin domains
    $allowedOrigins = array(
        '(http(s)://)?(www\.)?agencijapraktikum.me'
    );

    if (isset($_SERVER['HTTP_ORIGIN']) && $_SERVER['HTTP_ORIGIN'] != '') {
        foreach ($allowedOrigins as $allowedOrigin) {
        if (preg_match('#' . $allowedOrigin . '#', $_SERVER['HTTP_ORIGIN'])) {
            header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
            header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
            header('Access-Control-Max-Age: 1000');
            header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
            break;
        }
        }
    }

    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;
    use PHPMailer\PHPMailer\SMTP;

    if(!@require('PHPMailer/src/PHPMailer.php')) 
        throw new Exception("PHPMailer/src/PHPMailer.php");

    if(!@require('PHPMailer/src/Exception.php')) 
        throw new Exception("PHPMailer/src/Exception.php");
    
    if(!@require('PHPMailer/src/SMTP.php')) 
        throw new Exception("PHPMailer/src/SMTP.php");
    
    $email = $_POST["email"];
    $name = $_POST["name"];
    $message = $_POST["message"];
        

    $mail = new PHPMailer(true);

    try {
        $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      
        $mail->isSMTP();                                            
        $mail->Host       = 'smtp.gmail.com';                      // Set the SMTP server to send through e.g. smtp.gmail.com
        $mail->SMTPAuth   = true;            
        $mail->SMTPDebug = false;                             
        $mail->Username   = '{{INSERT EMAIL}}';               // Mail username
        $mail->Password   = '{{INSERT PWD}}';              // Mail password
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         
        $mail->Port       = 587;                                    
    
        $mail->setFrom('{{EMAIL}}', $email . '   ' . $name);
        $mail->addAddress('{{RECIPIENT}}', 'Agencija Praktikum doo');     // Add a recipient
    
        $mail->Subject = 'Message - Praktikum webpage';
        
        $mail->Body = $message; 
        $mail->IsHTML(true);
    
        $mail->send();

        echo json_encode('{"result" : "OK", "type" : "info", "message" : "Thank you for your message. We will respond you soon."}');
    
    } catch (Exception $e) {
        echo json_encode('{"result" : "NOK", "type" : "danger", "message" : "Error while sending message. Please, try later."}');
    }

 
